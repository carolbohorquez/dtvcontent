package com.directvgo.helper;

public class Constant {
    public static int SHOW = 8;
    public static int ASSET = 0;
    public static int RESOURCE = 1;
    public static int NAME = 3;
    public static int NAME_DTV = 1;
    public static int RESULT_SHOW = 19;
    public static int RESULT_EPISODE_SEASON = 21;
    public static int RESULT_SHOW_SEASON = 19;
    public static int ISP_ROOT_ASSET_ID = 16;
    public static String BR_MARKET = "BR";
    public static String EPISODE_FILE = "episode";
    public static String SHOW_FILE = "show";
    public static String TEST_TYPE_M = "metadata";
    public static String PROD = "prod";
    public static String STG = "stg";
    public static String MOVIE = "Movie";
}
