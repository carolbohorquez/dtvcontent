package com.directvgo.helper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class UtilProperties {
    private final static String fileName = "info.properties";


    public static <T> T getProperty(EProperty property) {
        try (InputStream input = UtilProperties.class.getClassLoader().getResourceAsStream(fileName)) {

            Properties prop = new Properties();

            if (input == null) {
                System.out.println("Sorry, unable to find config.properties");
                return null;
            }

            prop.load(input);
            return (T) prop.getProperty(property.value);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public enum EProperty {

        //REPORT PROPERTIES:

        REPORT_NAME("report.name"),
        REPORT_PATH("report.path"),
        CONTENT_PATH("content.info.path"),
        ORBIS_FILE_NAME("orbis.file.name"),
        ORBIS_FILE_NAME_PROD_PUBLISHED("orbis.file.name.prod.published"),
        ORBIS_FILE_NAME_PROD_UNPUBLISHED("orbis.file.name.prod.unpublished"),
        ORBIS_FILE_NAME_STG_PUBLISHED("orbis.file.name.stg.published"),
        ORBIS_FILE_NAME_STG_UNPUBLISHED("orbis.file.name.stg.unpublished"),
        FILE_ASSET_TO_TEST("file.asset.to.test"),

        RESULT_SHOW_LATAM_FILE_NAME("result.show.latam.file.name"),
        RESULT_EPISODE_LATAM_FILE_NAME("result.episode.latam.file.name"),
        RESULT_EPISODE_BR_FILE_NAME("result.episode.br.file.name"),
        RESULT_SHOW_BR_FILE_NAME("result.show.br.file.name"),

        ORBIS_URL_PROD_BR("orbis.url.prod.br"),
        ORBIS_URL_STG_BR("orbis.url.stg.br"),
        ORBIS_URL_PROD_LATAM("orbis.url.prod.latam"),
        ORBIS_URL_STG_LATAM("orbis.url.stg.latam"),
        ORBIS_URL_PROD_UNPUBLISHED("orbis.url.prod.unpublished"),
        ORBIS_URL_STG_UNPUBLISHED("orbis.url.stg.unpublished"),
        ORBIS_URL_PROD_PUBLISHED("orbis.url.prod.published"),
        ORBIS_URL_STG_PUBLISHED("orbis.url.stg.published"),


        //SETUP OPTIONS

        RESOURCE_TYPE("resource.type"),
        OPTION_RESOURCE_TYPE("option.resource.type"),
        MARKET("market"),
        TEST_TYPE("test.type"),
        ASSETS_PER_FILE("assets.per.file"),
        FORMAT_INFO("format.parse.info"),
        DOWNLOAD_ORBIS_FILE("download.orbis.file"),
        ORBIS_ENV("orbis.env"),


        FILE_NAME_PROD_BR("prod.file.name.br");


        private String value;

        EProperty(String value) {
            this.value = value;
        }
    }
}
