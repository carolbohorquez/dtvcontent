package com.directvgo.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.directvgo.helper.Constant.*;
import static com.directvgo.helper.UtilProperties.getProperty;

public class Util {

    public static String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }



    public static int getColumnNumber(String columnType) {
        int columnNum = 0;
        switch (columnType) {
            case "show":
                return SHOW;
            case "asset":
                return ASSET;
            case "resource":
                return RESOURCE;
            case "result show":
                return RESULT_SHOW;

        }

        return columnNum;
    }
}
