package com.directvgo.model.report;

public class Step {
    private String keyword;
    private String name;
    private Result result;
    private String line;

    public String getKeyword() {
        return keyword;
    }

    public String getName() {
        return name;
    }

    public Result getResult() {
        return result;
    }

    public String getLine() {
        return line;
    }
}
