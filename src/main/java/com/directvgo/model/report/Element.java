package com.directvgo.model.report;

import java.util.List;

public class Element {
    private String id;
    private String keyword;
    private String line;
    private String name;
    private List<Step> steps;

    public Element(String id, String keyword, String line, String name, List<Step> steps) {
        this.id = id;
        this.keyword = keyword;
        this.line = line;
        this.name = name;
        this.steps = steps;
    }

    public String getId() {
        return id;
    }

    public String getKeyword() {
        return keyword;
    }

    public String getLine() {
        return line;
    }

    public String getName() {
        return name;
    }

    public List<Step> getSteps() {
        return steps;
    }
}
