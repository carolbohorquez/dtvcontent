package com.directvgo.model.report;

public class Result {
    private String status;
    private String duration;
    private String error_message;

    public String getStatus() {
        return status;
    }

    public String getDuration() {
        return duration;
    }

    public String getError_message() {
        return error_message;
    }
}
