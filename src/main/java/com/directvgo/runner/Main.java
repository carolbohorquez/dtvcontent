package com.directvgo.runner;

import com.directvgo.script.FormatInput;
import com.directvgo.script.ParsePlaybackReport;

import java.io.IOException;

import static com.directvgo.helper.UtilProperties.EProperty.FORMAT_INFO;
import static com.directvgo.helper.UtilProperties.EProperty.REPORT_NAME;
import static com.directvgo.helper.UtilProperties.getProperty;

public class Main {

    public static void main(String[] args) {
        boolean formatInfo = Boolean.parseBoolean(getProperty(FORMAT_INFO));
        try {
            if (formatInfo) {
                FormatInput formatInput = new FormatInput();

                formatInput.addAssetToFile(formatInput.getAssetByResourceType());

            } else {
                ParsePlaybackReport parsePlaybackReport = new ParsePlaybackReport();
                parsePlaybackReport.convertJson(getProperty(REPORT_NAME));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
