package com.directvgo.script;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.directvgo.helper.Constant.*;
import static com.directvgo.helper.UtilProperties.EProperty.*;
import static com.directvgo.helper.UtilProperties.getProperty;

public class PublishAsset {
    private List<List<Object>> prodPublished;
    private static String name = "Pedro";
    List<List<Object>> prodUnpublished;
    List<List<Object>> stgPublished;
    List<List<Object>> stgUnpublished;
    List<List<Object>> assetsToTest;
    DataPreparation dataPreparation = new DataPreparation();
    List<Map.Entry<String, String>> listDuplicateByName;
    List<Map.Entry<String, String>> listDuplicateProd;

    public static String getName() {
        return name;
    }


    private String path = getProperty(CONTENT_PATH);

    private PublishAsset() throws IOException {
        this.downloadFiles();

        prodPublished = dataPreparation.prepareData(getProperty(ORBIS_FILE_NAME_PROD_PUBLISHED));
        prodUnpublished = dataPreparation.prepareData(getProperty(ORBIS_FILE_NAME_PROD_UNPUBLISHED));
        stgPublished = dataPreparation.prepareData(getProperty(ORBIS_FILE_NAME_STG_PUBLISHED));
        stgUnpublished = dataPreparation.prepareData(getProperty(ORBIS_FILE_NAME_STG_UNPUBLISHED));

    }

    private void downloadFiles() throws IOException {
        String fileNameProdPublished = path + "" + getProperty(ORBIS_FILE_NAME_PROD_PUBLISHED);
        String fileNameProdUnpublished = path + "" + getProperty(ORBIS_FILE_NAME_PROD_UNPUBLISHED);
        String fileNameStgPublished = path + "" + getProperty(ORBIS_FILE_NAME_STG_PUBLISHED);
        String fileNameStgUnpublished = path + "" + getProperty(ORBIS_FILE_NAME_STG_UNPUBLISHED);

        //dataPreparation.downloadFile(fileNameProdPublished, getProperty(ORBIS_URL_PROD_PUBLISHED));
        //dataPreparation.downloadFile(fileNameProdUnpublished, getProperty(ORBIS_URL_PROD_UNPUBLISHED));
        //dataPreparation.downloadFile(fileNameStgPublished, getProperty(ORBIS_URL_STG_PUBLISHED));
        //dataPreparation.downloadFile(fileNameStgUnpublished, getProperty(ORBIS_URL_STG_UNPUBLISHED));
    }

    public Map<String, String> getInfoFromFiles(List<List<Object>> list, int columnId, int columnValue) {
        Map<String, String> finalData = new HashMap<>();
        list = list.stream().filter(x -> x.get(RESOURCE).toString().equalsIgnoreCase(MOVIE)).collect(Collectors.toList());
        for (List data : list) {
            finalData.put(data.get(columnId).toString(), data.get(columnValue).toString());
        }
        return finalData;
    }

    public Map<String, String> getInfoFromFiles(List<List<Object>> list, int columnId, int columnValue, int columnName, String fileName) {
        Map<String, String> finalData = new HashMap<>();
        list = list.stream().filter(x -> x.get(RESOURCE).toString().equalsIgnoreCase(MOVIE)).collect(Collectors.toList());
        for (List data : list) {
            String value = "\"" + (data.get(columnName).toString()) + "\"," + data.get(columnValue).toString() + "," + fileName;
            finalData.put(data.get(columnId).toString(), value);
        }
        return finalData;
    }

    public List<String> getIdsToTest() {
        List<String> ids = new ArrayList<>();
        assetsToTest = dataPreparation.prepareData(getProperty(FILE_ASSET_TO_TEST));
        for (List asset : assetsToTest) {
            ids.add(asset.get(ASSET).toString());
        }
        return ids;
    }

    public List<String> getNamesToTest() {
        List<String> ids = new ArrayList<>();
        assetsToTest = dataPreparation.prepareData(getProperty(FILE_ASSET_TO_TEST));
        for (List asset : assetsToTest) {
            ids.add(asset.get(NAME_DTV).toString());
        }
        return ids;
    }

    public Map<String, String> createDataForProd() {
        Map<String, String> prodData = getInfoFromFiles(prodPublished, ASSET, ISP_ROOT_ASSET_ID);
        prodData.putAll(getInfoFromFiles(prodUnpublished, ASSET, ISP_ROOT_ASSET_ID));
        return prodData;
    }

    public Map<String, String> concatenateDataForProdDuplicate() {
        Map<String, String> prodData = getInfoFromFiles(prodPublished, ASSET, ISP_ROOT_ASSET_ID, NAME, "Published");
        prodData.putAll(getInfoFromFiles(prodUnpublished, ASSET, ISP_ROOT_ASSET_ID, NAME, "Unpublished"));
        return prodData;
    }

    public Map<String, String> createDataForStg() {
        Map<String, String> stgData = getInfoFromFiles(stgPublished, ISP_ROOT_ASSET_ID, ASSET);
        stgData.putAll(getInfoFromFiles(stgUnpublished, ISP_ROOT_ASSET_ID, ASSET));
        return stgData;
    }

    public Map<String, String> filterInProdByIds() {
        List<String> ids = getIdsToTest();
        Map<String, String> prodData = createDataForProd();
        Map<String, String> finalProdData = new HashMap<>();

        ids.forEach(x -> {
            finalProdData.put(prodData.get(x), x);
        });
        return finalProdData;
    }

    public Map<String, String> getProdDuplicatesByName(List<String> names) {
        Map<String, String> prodData = concatenateDataForProdDuplicate();
        Map<String, String> prodDataDuplicates = new HashMap<>();
        Map<String, String> dataToCompareProd = new HashMap<>();

        prodData.forEach((x, y) -> {
            String name = y.split("\",")[0];
            AtomicInteger i = new AtomicInteger();

            prodData.forEach((z, a) -> {

                String nameToCompare = a.split("\",")[0];
                if (name.equalsIgnoreCase(nameToCompare)) {
                    i.getAndIncrement();
                    if (i.get() >= 2) {
                        names.forEach(b -> {
                            if (b.equalsIgnoreCase(name.split("\"")[1])) {
                                prodDataDuplicates.put(z, a);
                                prodDataDuplicates.put(x, y);
                                dataToCompareProd.put(z, a.split("\",")[1]);
                                dataToCompareProd.put(x, y.split("\",")[1]);
                            }
                        });

                    }
                }
            });
        });

        listDuplicateByName = orderMap(prodDataDuplicates);
        return dataToCompareProd;
    }

    public List<Map.Entry<String, String>> orderMap(Map<String, String> prodDataDuplicates) {
        List<Map.Entry<String, String>> list = new ArrayList<>(prodDataDuplicates.entrySet());

        list.sort(Map.Entry.comparingByValue());

        return list;
    }

    public List<String> getStgIdsWithProdId(Map<String, String> prodData) {
        List<String> names = new ArrayList<>();

        Map<String, String> stgData = createDataForStg();
        List<String> finalStgData = new ArrayList<>();

        prodData.forEach((x, y) -> {
            finalStgData.add(stgData.get(x) + "|" + y);
        });


        return finalStgData;
    }

    public List<String> getStgIdsForDuplicates(List<Map.Entry<String, String>> prodDuplicates) {
        Map<String, String> stgData = createDataForStg();
        List<String> finalStgData = new ArrayList<>();
        prodDuplicates.forEach((x) -> {
            String id = x.getValue().split("\",")[1].split(",")[0];
            finalStgData.add(stgData.get(id));
        });
        return finalStgData;
    }

    public Map<String, String> getProdDuplicates() {

        Map<String, String> prodData = concatenateDataForProdDuplicate();
        Map<String, String> prodDataDuplicates = new HashMap<>();
        Map<String, String> dataToCompareProd = new HashMap<>();

        prodData.forEach((x, y) -> {
            String name = y.split("\",")[0];
            AtomicInteger i = new AtomicInteger();

            prodData.forEach((z, a) -> {

                String nameToCompare = a.split("\",")[0];
                if (name.equalsIgnoreCase(nameToCompare)) {
                    i.getAndIncrement();
                    if (i.get() >= 2) {
                        prodDataDuplicates.put(z, a);
                        prodDataDuplicates.put(x, y);
                        dataToCompareProd.put(z, a.split("\",")[1]);
                        dataToCompareProd.put(x, y.split("\",")[1]);
                    }
                }
            });
        });

        listDuplicateProd = orderMap(prodDataDuplicates);

        return dataToCompareProd;
    }

    public List<String> addingStgIdToDuplicates(List<String> stgIds, List<Map.Entry<String, String>> listDuplicates) {
        AtomicInteger i = new AtomicInteger();
        List<String> dataDuplicateComplete = new ArrayList<>();

        listDuplicates.forEach((x) -> {
            String[] values = x.getValue().split("\"");
            String[] values2 = values[2].split(",");

            String line = String.format("%s|%s|%s|%s|%s", x.getKey(), values[1], values2[1], stgIds.get(i.get()), values2[2]);
            dataDuplicateComplete.add(line);
            i.getAndIncrement();
        });

        return dataDuplicateComplete;
    }

    public List<String> removeIdsToTest(List<String> info) {
        Map<String, String> prodIdsToTest = filterInProdByIds();
        List<String> ids = new ArrayList<>();

        prodIdsToTest.forEach((x, y) -> {
            //AtomicInteger i = new AtomicInteger();
            AtomicInteger i = new AtomicInteger();
            info.forEach(z -> {
                String ispRootId = z.split("\\|")[2];
                if (x.equalsIgnoreCase(ispRootId)) {
                    ids.add(z);
                }
            });
        });

        int j = 0;
        while (j < ids.size()) {
            for (int k = 0; k < info.size(); k++) {
                if (ids.get(j).equalsIgnoreCase(info.get(k))) {
                    info.remove(k);

                }
            }
            j++;
        }

        return info;

    }

    public static void main(String[] args) throws IOException {
        PublishAsset publishAsset = new PublishAsset();

        List<String> names = publishAsset.getNamesToTest();
//        Map<String, String> val = publishAsset.getProdDuplicatesByName(publishAsset.getNamesToTest());
        Map<String, String> val = publishAsset.getProdDuplicates();
        Map<String, String> va2 = publishAsset.getProdDuplicatesByName(names);

        //System.out.println(stgIds.size());
        //System.out.println(val.size());

        //stgIds.forEach(System.out::println);


        // val.forEach((x, y) -> System.out.println(x + " - " + y));
        // publishAsset.listDuplicateByName.forEach((x) -> {
        //   System.out.println(x.getKey() + " - " + x.getValue());
        //});


        //PRINT ALL DUPLICATE INFO PROD
        List<String> stgIdsForDuplicates = publishAsset.getStgIdsForDuplicates(publishAsset.listDuplicateProd);
        List<String> dataDuplicateComplete = publishAsset.addingStgIdToDuplicates(stgIdsForDuplicates, publishAsset.listDuplicateProd);
        // dataDuplicateComplete.forEach(System.out::println);

        //PRINT DUPLICATE INFO FROM IDS TO TEST
        List<String> stgIdsForTest = publishAsset.getStgIdsForDuplicates(publishAsset.listDuplicateByName);
        List<String> dataDuplicateByName = publishAsset.addingStgIdToDuplicates(stgIdsForTest, publishAsset.listDuplicateByName);
        List<String> noTested = publishAsset.removeIdsToTest(dataDuplicateByName);
        //noTested.forEach(System.out::println);

        //PRINT STG IDS | PROD ID
        publishAsset.getStgIdsWithProdId(publishAsset.filterInProdByIds()).forEach(System.out::println);

    }
}
