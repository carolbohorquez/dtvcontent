package com.directvgo.script;

import com.directvgo.model.report.Element;
import com.directvgo.model.report.Step;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.directvgo.helper.Util.getDate;
import static com.directvgo.helper.UtilProperties.EProperty.REPORT_PATH;
import static com.directvgo.helper.UtilProperties.getProperty;

public class ParsePlaybackReport {
    String path = getProperty(REPORT_PATH);

    public void convertJson(String fileName) throws FileNotFoundException {

        String file = path + "" + fileName;

        JsonReader reader = new JsonReader(new FileReader(file));

        JsonArray convertedObject2 = new Gson().fromJson(reader, JsonArray.class);
        JsonObject convertedObject = new Gson().fromJson(convertedObject2.get(0).toString(), JsonObject.class);

        String elements = convertedObject.getAsJsonArray("elements").toString();

        Element[] element = new Gson().fromJson(elements, Element[].class);

        String name = "";
        String status = "";
        String errors = "";
        String resourceType = "";

        String date = getDate();

        ArrayList<String> results = new ArrayList<>();
        List<String> finalResults = new ArrayList<String>();

        for (Element element1 : element) {
            int count = 0;
            name = element1.getName().split("with id")[1].split("are")[0].trim();
            resourceType = element1.getName().split("live")[1].split("with id")[0].trim();
            for (Step step : element1.getSteps()) {
                if (step.getResult().getStatus().equalsIgnoreCase("passed")) {
                    status = "PASS";
                } else if (step.getResult().getStatus().equalsIgnoreCase("failed")) {
                    count++;
                    if (step.getName().contains("I should see all validations passed")) {
                        errors = step.getResult().getError_message().split("\n")[0].split("final:")[1].trim();

                        String arrErrors[] = errors.split(",");

                        for (String result : arrErrors) {
                            results.add(result);
                        }
                        break;
                    } else {
                        break;
                    }
                }
            }
            if (count == 0) {
                for (String id : name.split(",")) {
                    finalResults.add((id + "|" + status + "|" + date + "||" + resourceType).trim());
                }
            } else {
                for (String result : results) {
                    if (result.contains("PASS")) {
                        finalResults.add((result + "||" + resourceType).trim());

                    } else {
                        finalResults.add((result + "|" + resourceType).trim());

                    }
                }
            }

            results.clear();
        }

        List<String> list = finalResults.stream().distinct().sorted().collect(Collectors.toList());

        for (String res : list) {
            System.out.println(res);
        }
    }
}
