package com.directvgo.script;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import static com.directvgo.helper.Constant.ASSET;
import static com.directvgo.helper.Constant.TEST_TYPE_M;
import static com.directvgo.helper.UtilProperties.EProperty.*;
import static com.directvgo.helper.UtilProperties.getProperty;

public class FormatInput {

    private String path = getProperty(CONTENT_PATH);

    DataPreparation dataPreparation = new DataPreparation();

    private void addToFile(String filename, String asset, int fileCount) {
        File file = new File(path + "" + filename);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);

            if (file.createNewFile()) {
                writer.write("");
            }

            writer.write(asset);
            writer.close();
            System.out.println("Assets added to file! :) ");
            fileCount++;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Problems adding assets ");
        }
    }

    public void addAssetToFile(String data) {
        String testType = getProperty(TEST_TYPE).toString().toLowerCase();

        if (testType.equalsIgnoreCase(TEST_TYPE_M)) {
            String[] assets = data.split("\n");
            int fileCount = 1;
            for (String asset : assets) {
                String filename = "asset_" + getProperty(OPTION_RESOURCE_TYPE).toString().toLowerCase() + "_" + getProperty(MARKET).toString().toLowerCase() + "_" + fileCount + ".txt";
                addToFile(filename, asset, fileCount);
                fileCount++;

            }
        } else {
            String filename = "asset_" + getProperty(RESOURCE_TYPE).toString().toLowerCase() + "_" + getProperty(MARKET).toString().toLowerCase() + "_" + testType + ".txt";
            addToFile(filename, data, 1);
        }
    }

    private String printMetadataShow() throws IOException {
        List<String> input = dataPreparation.formatAssets(Integer.parseInt(getProperty(ASSETS_PER_FILE)), dataPreparation.getAssets(dataPreparation.getShowIds()), getProperty(TEST_TYPE).toString().toLowerCase());
        StringBuilder assets = new StringBuilder();

        for (String asset : input) {
            assets.append(asset).append("\n");
        }
        return assets.toString();
    }

    private String printMetadataSeason() throws IOException {
        List<String> input = dataPreparation.formatAssets(Integer.parseInt(getProperty(ASSETS_PER_FILE)), dataPreparation.getAssets(dataPreparation.getSeasonIds()), getProperty(TEST_TYPE).toString().toLowerCase());
        StringBuilder assets = new StringBuilder();

        for (String asset : input) {
            assets.append(asset).append("\n");
        }
        return assets.toString();
    }

    private String printAssetByTest() throws IOException {
        List<String> input = dataPreparation.formatAssets(Integer.parseInt(getProperty(ASSETS_PER_FILE)), dataPreparation.getAssets(getProperty(RESOURCE_TYPE), ASSET), getProperty(TEST_TYPE).toString().toLowerCase());
        StringBuilder assets = new StringBuilder();
        for (String asset : input) {
            assets.append(asset).append("\n");
        }
        return assets.toString();
    }

    public String getAssetByResourceType() throws IOException {
        String resourceType = getProperty(OPTION_RESOURCE_TYPE).toString().toLowerCase();
        String testType = getProperty(TEST_TYPE).toString().toLowerCase();
        boolean downloadFile = Boolean.parseBoolean(getProperty(DOWNLOAD_ORBIS_FILE));

        String fileName = path + "" + getProperty(ORBIS_FILE_NAME);

        String url = dataPreparation.getOrbisUrl();

        System.out.println(downloadFile);
        System.out.println(url);

        if (downloadFile) {
            dataPreparation.downloadFile(fileName, dataPreparation.getOrbisUrl());
        }

        System.out.println("DOWNLOAD FILE");

        if (testType.equalsIgnoreCase(TEST_TYPE_M)) {
            switch (resourceType) {
                case "show":
                    return printMetadataShow();
                case "season":
                    return printMetadataSeason();
                default:
                    return printAssetByTest();
            }
        } else {
            return printAssetByTest();
        }
    }
}
