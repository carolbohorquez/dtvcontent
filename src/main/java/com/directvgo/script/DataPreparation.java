package com.directvgo.script;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.directvgo.helper.Constant.*;
import static com.directvgo.helper.UtilProperties.EProperty.*;
import static com.directvgo.helper.UtilProperties.getProperty;

public class DataPreparation {

    private String path = getProperty(CONTENT_PATH);

    public List<List<Object>> prepareData(String filename) {

        if (filename.isEmpty()) {
            filename = getProperty(ORBIS_FILE_NAME);
        }

        List<List<Object>> dataLines = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path + "" + filename))) {
            String line;
            int i = 0;
            while (((line = br.readLine()) != null)) {
                i++;

                String[] values = line.split(",");
                if (!values[0].contains("Timestamp") && !values[0].contains("Orbis") && !values[0].contains("id") && !values[0].contains("ID") && !values[0].contains("PRODUCTION") && !values[0].contains("IFERROR")) {

                    List<Object> data = new ArrayList<>();

                    int index = 0;
                    int previous = 0;
                    boolean flag = false;

                    while (index != -1) {
                        index = line.indexOf(",", index);
                        if (index != -1) {
                            if (previous == 0) {
                                data.add(line.substring(previous, index));
                                flag = false;
                            }

                            if (line.indexOf(",", index + 1) == -1) {
                                data.add(line.substring(previous, line.length() - 1));
                                index = -1;
                            } else {

                                if (!flag && previous != 0) {
                                    data.add(line.substring(previous, index));
                                }
                                if (flag) {
                                    flag = false;
                                }
                                if (line.charAt(index + 1) == '"') {

                                    int c = getCompleteField(line, index + 1);
                                    data.add(line.substring(index + 2, c));
                                    index = c;
                                    previous = index + 1;
                                    flag = true;

                                } else {
                                    previous = index + 1;
                                    index++;

                                }

                            }
                        }
                    }
                    dataLines.add(data);
                }
            }

            return dataLines;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


//    public List<List<Object>> prepareDataFromDtvAssets(String filename) {
//
//        if (filename.isEmpty()) {
//            filename = getProperty(FILE_ASSET_TO_TEST);
//        }
//
//        List<List<Object>> dataLines = new ArrayList<>();
//
//        try (BufferedReader br = new BufferedReader(new FileReader(path + "" + filename))) {
//            String line;
//            int i = 0;
//            while (((line = br.readLine()) != null)) {
//                i++;
//
//                String[] values = line.split(",");
//                if (!values[0].contains("Orbis") && !values[0].contains("id") && !values[0].contains("ID") && !values[0].contains("PRODUCTION") && !values[0].contains("IFERROR")) {
//
//                    List<Object> data = new ArrayList<>();
//
//                    int index = 0;
//                    int previous = 0;
//                    boolean flag = false;
//
//                    while (index != -1) {
//                        index = line.indexOf(",", index);
//                        if (index != -1) {
//                            if (previous == 0) {
//                                data.add(line.substring(previous, index));
//                                flag = false;
//                            }
//
//                            if (line.indexOf(",", index + 1) == -1) {
//                                data.add(line.substring(previous, line.length() - 1));
//                                index = -1;
//                            } else {
//
//                                if (!flag && previous != 0) {
//                                    data.add(line.substring(previous, index));
//                                }
//                                if (flag) {
//                                    flag = false;
//                                }
//                                if (line.charAt(index + 1) == '"') {
//
//                                    int c = getCompleteField(line, index + 1);
//                                    data.add(line.substring(index + 2, c));
//                                    index = c;
//                                    previous = index + 1;
//                                    flag = true;
//
//                                } else {
//                                    previous = index + 1;
//                                    index++;
//
//                                }
//
//                            }
//                        }
//                    }
//                    dataLines.add(data);
//                }
//            }
//
//            return dataLines;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

    private int getCompleteField(String line, int index) {
        int a = line.indexOf('"', index + 1);
        int b = line.indexOf('"', a + 1);
        if ((b - a) == 1) {
            int c = line.indexOf('"', b);
            return getCompleteField(line, c);
        } else {
            return a;
        }

    }

    private int getNumLines(int sizeArray, int numAssets) {
        double numLinesDouble = ((double) sizeArray / (double) numAssets);
        int numLinesInt = (sizeArray / numAssets);
        return ((numLinesDouble - numLinesInt) == 0) ? numLinesInt : numLinesInt + 1;

    }

    private String getAssetsInOneLine(List<String> records, String testType, int cred, String type) {
        System.out.println("Aca");
        int k = records.size();
        String line = testType.equalsIgnoreCase("playback") ? "|" : "";
        for (String record : records) {
            String asset = record.split(",")[0].trim();
            if (k != 1) {
                line = line + asset + ",";
            } else {
                line = line + asset;
            }
            k--;

        }
        if (testType.equalsIgnoreCase("playback")) {
            line = line + "|" + (cred) + "|" + type.toLowerCase() + "|";
        }
        return line;
    }

    private String getFilesNameByMarket(String file) {
        if (getProperty(MARKET) != BR_MARKET) {
            System.out.println(file);
            switch (file) {
                case "episode":
                    return getProperty(RESULT_EPISODE_BR_FILE_NAME);
                case "show":
                    return getProperty(RESULT_SHOW_BR_FILE_NAME);
                default:
                    throw new IllegalStateException("Unexpected value: " + file);
            }

        } else {
            switch (file) {
                case "episode":
                    return getProperty(RESULT_EPISODE_LATAM_FILE_NAME);
                case "show":
                    return getProperty(RESULT_SHOW_LATAM_FILE_NAME);
                default:
                    throw new IllegalStateException("Unexpected value: " + file);
            }
        }
    }

    public void downloadFile(String fileName, String fileUrl) throws IOException {
        FileUtils.copyURLToFile(new URL(fileUrl), new File(fileName));
    }

    public List<String> getShowIds() {
        List<List<Object>> orbisAssets = prepareData("");
        List<List<Object>> episodeResultAssets = prepareData(getFilesNameByMarket(EPISODE_FILE));
        List<String> records = getIdsByEpisode(episodeResultAssets, RESULT_SHOW);

        assert orbisAssets != null;

        for (List asset : orbisAssets) {
            if (((String) asset.get(RESOURCE)).equalsIgnoreCase(getProperty(RESOURCE_TYPE))) {
                records.add((String) asset.get(SHOW));
            }
        }

        records = records.stream().distinct().filter(x -> !x.isEmpty()).collect(Collectors.toList());

        return records;
    }

    private List<String> getIdsByEpisode(List<List<Object>> episodeResultAssets, int idType) {
        List<String> records = new ArrayList<>();
        assert episodeResultAssets != null;
        for (List asset : episodeResultAssets) {
            records.add((String) asset.get(idType));
        }
        return records;
    }

    public List<String> getSeasonIds() {
        List<List<Object>> episodeResultAssets = prepareData(getFilesNameByMarket(EPISODE_FILE));
        List<String> records = getIdsByEpisode(episodeResultAssets, RESULT_EPISODE_SEASON);

        List<List<Object>> showResultAssets = prepareData(getFilesNameByMarket(SHOW_FILE));


        assert showResultAssets != null;

        for (List asset : showResultAssets) {
            String seasons = (String) asset.get(RESULT_SHOW_SEASON);

            for (String ids : seasons.split(",")) {
                records.add(ids.trim());
            }
        }

        records = records.stream().distinct().filter(x -> !x.isEmpty()).collect(Collectors.toList());

        return records;
    }

    public String getOrbisUrl() {
        String market = getProperty(MARKET).toString().toUpperCase();
        String env = getProperty(ORBIS_ENV).toString().toLowerCase();
        String orbisUrl = "";

        switch (env) {
            case "stg":
                if (market.equalsIgnoreCase(BR_MARKET)) {
                    orbisUrl = getProperty(ORBIS_URL_STG_BR);
                } else {
                    orbisUrl = getProperty(ORBIS_URL_STG_LATAM);
                }
                break;
            default:
                if (market.equalsIgnoreCase(BR_MARKET)) {
                    orbisUrl = getProperty(ORBIS_URL_PROD_BR);
                } else {
                    orbisUrl = getProperty(ORBIS_URL_PROD_LATAM);
                }
                break;
        }

        return orbisUrl;
    }

    public List<String> getAssets(String resourceType, Integer idType) throws IOException {
        List<List<Object>> records = prepareData("");
        List<String> assetList = new ArrayList<>();

        for (List data : records) {
            String columnResource = (String) data.get(RESOURCE);

            if (columnResource.equalsIgnoreCase(resourceType)) {
                if (!((String) data.get(idType)).isEmpty()) {
                    assetList.add(data.get(idType) + "," + resourceType);
                }
            }
        }

        return assetList;
    }

    public List<String> getAssets(List<String> assetList) throws IOException {

        System.out.println("SIZE: " + assetList.size());
        List<String> finalAssetList = new ArrayList<>();
        String resourceType = getProperty(RESOURCE_TYPE);

        for (String asset : assetList) {
            finalAssetList.add(asset + "," + resourceType);
        }

        return finalAssetList;
    }

    public List<String> formatAssets(int numAssets, List<String> records, String testType) {
        List<String> lines = new ArrayList<>();

        if (records.size() == 0) {
            return lines;
        }

        records = records.stream().distinct().collect(Collectors.toList());

        String type = records.get(0).split(",")[1];
        int cred = 1;


        if (records.size() > numAssets && numAssets != 0) {
            int numLines = getNumLines(records.size(), numAssets);
            for (int i = 0; i < numLines; i++) {
                String line = testType.equalsIgnoreCase("playback") ? "|" : "";
                if (records.size() > numAssets) {
                    for (int j = 0; j < numAssets; j++) {
                        String asset = records.get(j).split(",")[0];
                        if (j != numAssets - 1) {
                            line = line + asset + ",";
                        } else {
                            line = line + asset;
                        }
                    }

                    records = records.subList(numAssets, (records.size()));

                    if (cred == 10) {
                        cred = 1;
                    }

                    if (testType.equalsIgnoreCase("playback")) {
                        line = line + "|" + (cred) + "|" + type.toLowerCase() + "|";
                    }

                } else {
                    lines.add(getAssetsInOneLine(records, testType, cred, type));
                }

                lines.add(line);
                cred++;
            }
        } else {
            lines.add(getAssetsInOneLine(records, testType, cred, type));
        }
        return lines;
    }
}
